/*
 *
 *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  SPDX-License-Identifier: Apache-2.0
 *
 */
import React, { useEffect } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { Button, Form, Input, Modal } from 'tea-component';
import formUtils, { Validator } from 'src/utils/form-utils';
import { useFetchGenerateCert } from 'src/common/apis/certificates/hooks';
import { renderChainAlgorithmFormItem } from './import-org-cert-modal';
import { ChainAlgorithm, ICertItem } from '../../../../common/apis/certificates/interface';

const { getStatus } = formUtils;

interface IGenerateCertModalParam {
  visible: boolean;
  close: () => void;
  successCallback?: () => void;
}

interface IInitialData {
  OrgId: string;
  OrgName: string;
  Algorithm: string;
}

const initialData: IInitialData = {
  OrgId: '',
  OrgName: '',
  Algorithm: ChainAlgorithm.CN.toString(),
};

export function GenerateOrgCertModal({ visible, close, successCallback }: IGenerateCertModalParam) {
  const {
    control,
    handleSubmit,
    reset,
    formState: { errors, isValidating, isSubmitted, isValid },
  } = useForm({
    mode: 'onBlur',
    defaultValues: {
      Algorithm: ChainAlgorithm.CN.toString(),
    } as any,
  });
  const { fetch } = useFetchGenerateCert();

  const submit = async (values: IInitialData) => {
    await fetch({
      ...values,
      CertType: 0,
      Algorithm: +values.Algorithm as ICertItem['Algorithm'],
    });
    successCallback?.();
  };

  useEffect(() => {
    if (visible) {
      reset();
    }
  }, [visible]);

  return (
    <Modal caption="申请组织证书" visible={visible} onClose={close}>
      <Modal.Body>
        <Form>
          <Controller
            name="OrgId"
            control={control}
            defaultValue={initialData.OrgId}
            rules={{
              validate: (OrgId) => Validator.validateOrgId(OrgId),
            }}
            render={({ field, fieldState }) => (
              <Form.Item
                label="组织ID"
                required
                status={isValidating ? 'validating' : getStatus({ fieldState, isSubmitted, isValidating })}
                message={errors.OrgId?.message}
              >
                <Input placeholder="请输入组织ID" size="full" {...field} />
              </Form.Item>
            )}
          />
          <Controller
            name="OrgName"
            control={control}
            defaultValue={initialData.OrgName}
            rules={{
              validate: (OrgName) => Validator.validateOrgName(OrgName),
            }}
            render={({ field, fieldState }) => (
              <Form.Item
                label="组织名称"
                required
                status={isValidating ? 'validating' : getStatus({ fieldState, isSubmitted, isValidating })}
                message={errors.OrgName?.message}
              >
                <Input placeholder="请输入组织名称" size="full" {...field} />
              </Form.Item>
            )}
          />
          {renderChainAlgorithmFormItem(control, isSubmitted, isValidating)}
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button type="primary" disabled={!isValid} onClick={handleSubmit(submit)}>
          确定
        </Button>
        <Button onClick={close}>取消</Button>
      </Modal.Footer>
    </Modal>
  );
}
