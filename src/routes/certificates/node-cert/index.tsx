/*
 *
 *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  SPDX-License-Identifier: Apache-2.0
 *
 */
import React, { useCallback, useEffect, useState } from 'react';
import { Button, Card, InputAdornment, Justify, PagingQuery, SearchBox, Table, TableColumn } from 'tea-component';
import { formatDate } from 'src/utils/date';
import { ChainAlgorithm, ICertItem, IDetailCertParam } from 'src/common/apis/certificates/interface';
import { GenerateNodeCertModal } from './modals/generate-node-cert-modal';
import { ImportNodeCertModal } from './modals/import-node-cert-modal';
import CertMultipleDetailModal from '../modals/cert-multiple-detail-modal';
import { useFetchCertList, useFetchRemoveCertDetail } from 'src/common/apis/certificates/hooks';
import { PAGE_SIZE_OPTIONS } from 'src/utils/common';
import { Select } from 'tea-component/es';

const { pageable, autotip } = Table.addons;

const CERT_TYPE_MAP: {
  [index: number]: string;
} = {
  0: '根证书',
  1: 'ca证书',
  2: '用户admin证书',
  3: '用户client证书',
  4: '共识节点',
  5: '同步节点',
};

const searchOptions = [
  {
    text: '节点名称',
    value: 'NodeName',
  },
  {
    text: '组织名称',
    value: 'OrgName',
  },
];

export function renderChainAlgorithm({ Algorithm }: ICertItem) {
  return Algorithm === ChainAlgorithm.CN ? '国密' : '非国密';
}

export function NodeCert() {
  const [generateModalVisible, setGenerateModalVisible] = useState<boolean>(false);
  const [importModalVisible, setImportModalVisible] = useState<boolean>(false);
  const [detailModalVisible, setDetailModalVisible] = useState<boolean>(false);
  const [targetCert, setTargetCert] = useState<ICertItem | null>(null);
  const [pageQuery, setPageQuery] = useState<Required<PagingQuery>>({ pageSize: 10, pageIndex: 1 });
  const [searchValue, setSearchValue] = useState<string>('');
  const [searchField, setSearchField] = useState<string>('NodeName');
  const [searchValueInput, setSearchValueInput] = useState<string>('');
  const { list, totalCount, fetch } = useFetchCertList();
  const { fetchRemoveCert } = useFetchRemoveCertDetail();

  const fetchCertList = useCallback(() => {
    fetch({
      Type: 1,
      [searchField]: searchValue ?? '',
      PageNum: pageQuery.pageIndex - 1,
      PageSize: pageQuery.pageSize,
      ChainMode: 'permissionedWithCert',
    });
  }, [fetch, searchValue, pageQuery, searchField]);

  const fetchDeleteCert = useCallback(
    (param: IDetailCertParam) => {
      fetchRemoveCert(param).then(() => {
        fetchCertList();
      });
    },
    [fetchCertList],
  );

  useEffect(() => {
    fetchCertList();
  }, [fetchCertList]);

  const defaultColumns: TableColumn<ICertItem>[] = [
    {
      key: 'NodeName',
      header: '节点名称',
    },
    {
      key: 'OrgName',
      header: '所属组织',
    },
    {
      key: 'CertType',
      header: '节点角色',
      render: ({ CertType }) => CERT_TYPE_MAP[CertType],
    },
    {
      key: 'Algorithm',
      header: '密码算法',
      render: renderChainAlgorithm,
    },
    {
      key: 'CreateTime',
      header: '创建时间',
      render: ({ CreateTime }) => formatDate(CreateTime),
    },
    {
      key: 'Action',
      header: '操作',
      // eslint-disable-next-line react/display-name
      render: (record) => (
        <>
          <Button
            type="link"
            onClick={() => {
              setTargetCert(record);
              setDetailModalVisible(true);
            }}
          >
            查看
          </Button>
          <Button
            type="link"
            onClick={() => {
              fetchDeleteCert({ CertId: record.Id });
            }}
          >
            删除
          </Button>
        </>
      ),
    },
  ];

  return (
    <>
      <GenerateNodeCertModal
        visible={generateModalVisible}
        close={() => setGenerateModalVisible(false)}
        successCallback={() => {
          setGenerateModalVisible(false);
          fetchCertList();
        }}
      />
      <ImportNodeCertModal
        visible={importModalVisible}
        close={() => setImportModalVisible(false)}
        successCallback={() => {
          setImportModalVisible(false);
          fetchCertList();
        }}
      />
      <CertMultipleDetailModal
        zipName={targetCert?.NodeName}
        visible={detailModalVisible}
        CertType={1}
        CertId={targetCert?.Id}
        close={() => setDetailModalVisible(false)}
      />
      <Justify
        left={
          <>
            <Button type="primary" onClick={() => setGenerateModalVisible(true)}>
              申请节点证书
            </Button>
            <Button type="primary" onClick={() => setImportModalVisible(true)}>
              导入节点证书
            </Button>
          </>
        }
        right={
          <InputAdornment
            before={
              <Select
                options={searchOptions}
                defaultValue="NodeName"
                style={{ width: 'auto' }}
                onChange={setSearchField}
              />
            }
          >
            <SearchBox
              placeholder={`请输入关键词`}
              size="l"
              value={searchValueInput}
              onChange={(value) => setSearchValueInput(value)}
              onSearch={(value) => {
                setSearchValue(value ?? '');
                setPageQuery({
                  ...pageQuery,
                  pageIndex: 1,
                });
              }}
              onClear={() => {
                setSearchValue('');
                setPageQuery({
                  ...pageQuery,
                  pageIndex: 1,
                });
              }}
            />
          </InputAdornment>
        }
      />
      <Card className="tea-mt-5n">
        <Table
          records={list}
          recordKey={(record) => `${record.Id}--${record.CertUse}`}
          columns={defaultColumns}
          addons={[
            pageable({
              recordCount: totalCount,
              pageIndex: pageQuery.pageIndex,
              pageSize: pageQuery.pageSize,
              pageSizeOptions: PAGE_SIZE_OPTIONS,
              onPagingChange: ({ pageIndex, pageSize }) =>
                setPageQuery({ pageIndex: pageIndex ?? 1, pageSize: pageSize ?? 10 }),
            }),
            autotip({
              emptyText: '暂无数据',
              isFound: !!searchValue.length,
              onClear: () => {
                setSearchValue('');
                setSearchValueInput('');
              },
            }),
          ]}
        />
      </Card>
    </>
  );
}
